import re

value = []

def user_input():
    """
    Taking user/console inputs.
    """
    passwords=[x for x in input("enter the password ").split(',')]
    validate_password(passwords)
    print(",".join(value))

def validate_password(passwords):
    """
    Validating given password.
    """
    for p in passwords:
        if len(p)<4 or len(p)>6:
            continue
        if not re.search("[a-z]",p):
            print(" a-z characters  not in the password")
            continue
        elif not re.search("[0-9]",p):
            print("0-9 letters are not in password")
            continue
        elif not re.search("[A-Z]",p):
            print("A-Z alphabets are not in password")
            continue
        elif not re.search("[*#+@]",p):
            print("symbols are not in password")
            continue
        elif re.search(" +",p):
            continue
        else:
            value.append(p)

# Calling function.
user_input()